const values = [
[
    180, 161, 102, 31, 31, 46, 175, 199, 106, 89, 137, 200, 199, 126, 159, 114,
    184, 198, 10, 183, 30, 150, 47, 187, 35, 126, 131, 159, 173, 178, 174, 69,
    87, 157, 75, 190, 181, 122, 65, 19, 63, 67, 56, 115, 65, 162, 159, 144, 77,
    36,
],
[
    195, 31, 102, 148, 119, 28, 190, 197, 145, 190, 84, 91, 62, 101, 72, 178,
    81, 92, 119, 103, 183, 57, 122, 47, 170, 12, 19, 193, 196, 199, 47, 43, 85,
    152, 158, 151, 42, 26, 112, 44, 163, 186, 50, 121, 152, 49, 169, 136, 198,
    98,
],
[
    26, 98, 102, 120, 105, 153, 47, 167, 188, 41, 121, 109, 16, 199, 83, 196,
    188, 101, 122, 121, 193, 59, 77, 27, 43, 55, 70, 186, 24, 118, 185, 63, 122,
    68, 119, 0, 16, 44, 181, 135, 102, 43, 134, 91, 180, 152, 94, 169, 110, 96,
],
];

// Constantes
const ROUGE = 2
const VERT = 1
const DEFAULT = 0
let arrColor = []

/**
 * Define if a color is used more than 2 times for green and more than 1 time for Red 
 * Iterate left to right for green
 * Iterate right to left for red
 * @param {*} array 
 */
function defineColor(array) {
    
    // Object to save how many time number has been colored
    let objColor = {}

    // iterate from left to right for GREEN
    array.map((arr_numbers,index) => {
        //contain every list of numbers with value and color
        let kColonne = [];
        
        //each numbers
        arr_numbers.map((value)=> {
            //set default color to define later if its red
            let color = DEFAULT
            if(value > 100) {
                //if the value exist and if the value greater to 2 (for green)
                if(objColor[value] && objColor[value] < 2) {
                    color = VERT 
                    //increment evey time we see the number
                    objColor[value]++
                //if we havent seen the value , we set it to red
                } else if(!objColor[value]) {
                    color = VERT
                    //set the value
                    objColor[value] = 1
                }
                else {
                    color = DEFAULT
                }
            }
            // push the value into the column
            kColonne.push({
                value: value,
                color: color
            })
        })
        // push the column into array
        arrColor.push(kColonne);
    })

    // Revert all column and start to the last one
    array.slice(0).reverse().map((arr_numbers,index) => {
        //revert all number and start to the last 
        arr_numbers.slice(0).reverse().map((value,k_numer)=> {
            //Same logic as green
            let color = DEFAULT
            if(value < 100) {
                if(objColor[value]) {
                    color = DEFAULT
                }
                else {
                    color = ROUGE
                    objColor[value] = 1
                }
                // to find the real index of the column we have to take the amount of numbers minus index of current inverted array
                //then set the color
                arrColor[values.length-index-1][arr_numbers.length-k_numer-1].color = color;
            }
        })
    })
}


defineColor(values)



let table = document.querySelector('table');


let nbLignes = 0;
/**
 * Foreach every column in values array 
 */
values.map(listN => {
    //get the maximum amount of line of each column 
    if(nbLignes < listN.length) {
        nbLignes = listN.length;
    }
})

//iterates each lines to create TR before TD
for(let i = 0; i < nbLignes; i++) {
    let tr  = document.createElement('tr');

    //iterate through the array with color informations
    arrColor.map((listN) => {
        let td  = document.createElement('td');
        //if the column as at least the required amount of line 
        if(i <= listN.length-1) {
            //add the value
            td.innerHTML = listN[i].value;
            //add the color
            td.style.backgroundColor = listN[i].color === VERT ? 'green' : (listN[i].color === ROUGE ? 'red' : 'transparent');
        }
        tr.append(td);
    });
    table.append(tr);
}


